#include "common.c"


int main(int argc,char** argv){

	//récupération de la clé de la MQ
	key_t clef=ftok(".mq",0);

	if((int)clef==-1){
		perror("Erreur : ");
		return -1;
	}else{
		printf("Clé ftok : %d\n",(int)clef);
	}

	//identification de la MQ
	int messageQueueID=msgget(clef,IPC_CREAT|0666);

	if(messageQueueID<0){
		perror("Erreur : ");
		return -1;
	}else{
		printf("ID de la file de message : %d\n",messageQueueID);
	}

	query q; //struct reçue
	int queryLength=sizeof(q)-sizeof(q.mType);

	response r; //struct qui sera renvoyée
	int responseLength=sizeof(r.result);
	
	
	int receptionMessage;
	int envoiMessage;
	while(1){
		receptionMessage=msgrcv(messageQueueID,&q,queryLength,3,0);
		if(receptionMessage<0){
			perror("Erreur : ");
			return -1;
		}else{
			/*printf("Message bien reçu\n");
			printf("--struct query--\n");
			printf("operation : %f %c %f\n\n",q.operande[0],q.operateur,q.operande[1]);*/
		}

		printf("pid reçu : %d\n",q.pid);
		r.result=q.operande[0]*q.operande[1];

		r.mType=(long)q.pid;

		envoiMessage=msgsnd(messageQueueID,&r,responseLength,0);

		if(envoiMessage<0){
			perror("Erreur : ");
			return -1;
		}else{
			printf("Le message a bien été envoyé\n");
		}

	}

	return 0;
}