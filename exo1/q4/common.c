#include <errno.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h> //printf, scanf...
#include <stdlib.h> //malloc, atoi... 
#include <unistd.h> //sleep
#include <time.h>

//----STRUCT-----

typedef struct response{
	long mType;
	float result;
}response;

typedef struct query{
	long mType;
	float operande[2];
	int pid;
}query;


void printQuery(query q){
	printf("mType : %ld\nOpérandes : %f,%f\nPid : %d\n",q.mType,q.operande[0],q.operande[1],q.pid);
}