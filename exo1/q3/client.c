#include <errno.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h> //printf, scanf...
#include <stdlib.h> //malloc, atoi... 
#include <unistd.h> //sleep
#include <time.h>

//----STRUCT-----

typedef struct response{
	long mType;
	float result;
}response;

typedef struct query{
	long mType;
	float operande[2];
	char operateur;
	int pid;
}query;

//----FUNCTIONS----

void printQuery(query q){
	printf("mType : %ld\nOpérandes : %f,%f\nOpérateur : %c\nPid : %d\n",q.mType,q.operande[0],q.operande[1],q.operateur,q.pid);
}

char getOperateur(int number){
	char operateur;
	switch(number%4){
		case 0:
			operateur='+';
			break;

		case 1:
			operateur='-';
			break;

		case 2:
			operateur='*';
			break;

		case 3:
			operateur='/';
			break;

		default:
			operateur='+';
	}
	return operateur;
}


int main(int argc,char** argv){

	if(argc<2){
		printf("Erreur : argument manquant : nombre de requêtes à envoyer\n");
		return -1;
	}

	int queryNumber=atoi(argv[1]);
	time_t t;
	srand((unsigned)time(&t));

	//récupération de la clé de la MQ
	key_t clef=ftok(".mq",0);

	if((int)clef==-1){
		perror("Erreur : ");
		return -1;
	}else{
		printf("Clé ftok : %d\n",(int)clef);
	}

	//identification de la MQ
	int messageQueueID=msgget(clef,0666);

	if(messageQueueID<0){
		perror("Erreur : ");
		return -1;
	}else{
		printf("ID de la file de message : %d\n",messageQueueID);
	}

	//préparation du message
	//long myPid=(long)getpid();
	query operation;
	int envoiMessage;
	int messageLength=sizeof(operation)-sizeof(operation.mType);
	operation.mType=1;
	while(queryNumber>0){
		
		operation.operande[0]=rand()%20;
		operation.operande[1]=rand()%20;
		operation.operateur=getOperateur(rand());
		operation.pid=getpid();

		printQuery(operation);

		//envoi de message
		envoiMessage=msgsnd(messageQueueID,&operation,messageLength,0);

		if(envoiMessage<0){
			perror("Erreur : ");
			return -1;
		}else{
			printf("Le message a bien été envoyé\n");
		}

		response r;
		int receptionMessage=msgrcv(messageQueueID,&r,sizeof(r.result),getpid(),0);

		if(receptionMessage<0){
			perror("Erreur : ");
			return -1;
		}

		printf("Résultat reçu : %f\n",r.result);
		queryNumber--;
	}

	return 0;
}