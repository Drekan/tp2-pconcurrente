#include <errno.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h> //printf, scanf...
#include <stdlib.h> //malloc, atoi... 
#include <unistd.h> //sleep

//----STRUCT-----

typedef struct response{
	long mType;
	float result;
}response;

typedef struct query{
	long mType;
	float operande[2];
	char operateur;
}query;

int main(int argc,char** argv){

	//récupération de la clé de la MQ
	key_t clef=ftok(".mq",0);

	if((int)clef==-1){
		perror("Erreur : ");
		return -1;
	}else{
		printf("Clé ftok : %d\n",(int)clef);
	}

	//identification de la MQ
	int messageQueueID=msgget(clef,IPC_CREAT|0666);

	if(messageQueueID<0){
		perror("Erreur : ");
		return -1;
	}else{
		printf("ID de la file de message : %d\n",messageQueueID);
	}

	query q; //struct reçue
	int queryLength=sizeof(q.operande)+sizeof(q.operateur);

	response r; //struct qui sera renvoyée
	int responseLength=sizeof(r.result);
	r.mType=2;
	
	
	int receptionMessage;
	int envoiMessage;
	while(1){
		receptionMessage=msgrcv(messageQueueID,&q,queryLength,1,0);
		if(receptionMessage<0){
			perror("Erreur : ");
			return -1;
		}else{
			printf("Message bien reçu\n");
			printf("--struct query--\n");
			printf("operation : %f %c %f\n\n",q.operande[0],q.operateur,q.operande[1]);
		}

		switch(q.operateur){
			case '+':
				r.result=q.operande[0]+q.operande[1];
				break;

			case '-':
				r.result=q.operande[0]-q.operande[1];
				break;

			case '*':
				r.result=q.operande[0]*q.operande[1];
				break;

			case '/':
				r.result=q.operande[0]/q.operande[1];
				break;

			default:
				r.result=0;
		};

		envoiMessage=msgsnd(messageQueueID,&r,responseLength,0);

		if(envoiMessage<0){
			perror("Erreur : ");
			return -1;
		}else{
			printf("Le message a bien été envoyé\n");
		}

	}

	return 0;
}